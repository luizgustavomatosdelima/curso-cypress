describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));
    it("fills all the text input fields", () =>{
        const fistName = "Luiz";
        const lastName = "Lima"
         cy.get("#first-name").type(fistName);
         cy.get("#last-name").type(lastName);
         cy.get("#email").type("lg@lg.com");
         cy.get("#requests").type("Vegetarian");
         cy.get("#signature").type(`${fistName} ${lastName}`);
    });

    it("Select two tickets",() => {
        cy.get("#ticket-quantity").select("2");
    });
    it("Select 'vip' ticket type",() =>{
        cy.get("#vip").check();
    });
    it("Selects 'social media' checkbox",()=>{
        cy.get("#social-media").check();
    });
    it("Selects 'friend', and 'publication', then uncheck 'friend'",()=>{
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", ()=>{
        cy.get("#email")
        .as("email")
        .type("lg-lg.com");

        cy.get("#email.invalid")
        .as("invalidEmail")
        .should("exist");

        cy.get("@email").clear().type("lg@lg.com");

        cy.get("#email.invalid").should("not.exist");
    });

    it("fills and reset the form",()=>{
        const fistName = "Luiz";
        const lastName = "Lima"
        const fullName = `${fistName} ${lastName}`
         cy.get("#first-name").type(fistName);
         cy.get("#last-name").type(lastName);
         cy.get("#email").type("lg@lg.com");
         cy.get("#ticket-quantity").select("2");
         cy.get("#vip").check();
         cy.get("#friend").check();
         cy.get("#requests").type("IPA beer");

         cy.get(".agreement p").should(
             "contain",
             `I, ${fullName}, wish to buy 2 VIP tickets`,
        );
        
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

            cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it.only("fills mandatory fields using support command", ()=>{
        const custumer = {
            firstName: "john",
            lastName: "Carry",
            email: "john_carry@gmail.com"
        }
        cy.fillMandatoryFields(custumer);
        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

            cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });
});